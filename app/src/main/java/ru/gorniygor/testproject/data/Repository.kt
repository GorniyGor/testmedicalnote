package ru.gorniygor.testproject.data

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.gorniygor.testproject.data.api.Api
import ru.gorniygor.testproject.domain.Item
import ru.gorniygor.testproject.domain.repository.IRepository
import java.util.concurrent.TimeUnit

object Repository : IRepository {

    val api : Api = Retrofit.Builder()
            .baseUrl("https://google.com")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(Api::class.java)

    override fun getData(): Observable<List<Item>> {
        return Observable.interval(15, TimeUnit.SECONDS)
                .flatMap { api.getItems() }
                .map { it.stock }
    }
}