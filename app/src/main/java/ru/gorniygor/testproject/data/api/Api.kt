package ru.gorniygor.testproject.data.api

import io.reactivex.Observable
import retrofit2.http.GET

interface Api {

    @GET("http://phisix-api3.appspot.com/stocks.json")
    fun getItems() : Observable<StocksResponse>
}