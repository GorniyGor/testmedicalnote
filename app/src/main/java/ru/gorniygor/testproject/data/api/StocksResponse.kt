package ru.gorniygor.testproject.data.api

import ru.gorniygor.testproject.domain.Item

data class StocksResponse (
        val stock: List<Item>
)