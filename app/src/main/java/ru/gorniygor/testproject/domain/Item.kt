package ru.gorniygor.testproject.domain

import com.google.gson.annotations.SerializedName

class Item(
        val name: String = "",
        val price: Price,
        val volume: Int,
        val symbol: String,
        @SerializedName("percent_change")
        val percentChange: Double
)