package ru.gorniygor.testproject.domain

data class Price(
        val currency: String,
        val amount: Double )