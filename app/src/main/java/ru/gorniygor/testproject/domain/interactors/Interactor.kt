package ru.gorniygor.testproject.domain.interactors

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.gorniygor.testproject.domain.Item
import ru.gorniygor.testproject.domain.repository.IRepository

/**
 * Created by Gor on 24.06.2018.
 */
class Interactor(private val repository: IRepository) {

    fun getData(): Observable<List<Item>>{
        return repository.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}