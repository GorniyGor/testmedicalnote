package ru.gorniygor.testproject.domain.repository

import io.reactivex.Observable
import ru.gorniygor.testproject.domain.Item

interface IRepository {
    fun getData(): Observable<List<Item>>
}