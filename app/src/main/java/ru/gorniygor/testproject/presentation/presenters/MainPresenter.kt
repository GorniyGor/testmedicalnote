package ru.gorniygor.testproject.presentation.presenters

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.gorniygor.testproject.data.Repository
import ru.gorniygor.testproject.domain.interactors.Interactor
import ru.gorniygor.testproject.presentation.views.iviews.MainView

@InjectViewState
class MainPresenter : MvpPresenter<MainView>() {
    private val interactor = Interactor(Repository)

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadData()
    }

    fun loadData() {
        interactor.getData()
                .doOnSubscribe { viewState.showProgress() }
                .doOnEach { viewState.hideProgress() }
                .subscribe(
                        { viewState.setData(it) },
                        { viewState.error(it.message) }
                )
    }

}