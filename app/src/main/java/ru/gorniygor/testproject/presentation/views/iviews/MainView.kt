package ru.gorniygor.testproject.presentation.views.iviews

import ru.gorniygor.testproject.domain.Item

interface MainView : BaseView {
    fun setData(data: List<Item>)
}