package ru.gorniygor.testproject.presentation.views.ui.activities

import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_main.*
import ru.gorniygor.testproject.R
import ru.gorniygor.testproject.domain.Item
import ru.gorniygor.testproject.presentation.presenters.MainPresenter
import ru.gorniygor.testproject.presentation.views.iviews.MainView
import ru.gorniygor.testproject.presentation.views.ui.holders.ItemAdapter
import ru.gorniygor.testproject.util.DiffUtilCallback

class MainActivity : BaseActivity(), MainView {

    @InjectPresenter
    lateinit var presenter: MainPresenter

    private val adapter = ItemAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView.layoutManager = GridLayoutManager(this, 2)
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(DividerItemDecoration(this, GridLayoutManager.VERTICAL))
        recyclerView.setHasFixedSize(true)
    }

    override fun setData(data: List<Item>) {
        val diffUtilsCallback = DiffUtilCallback(ArrayList(adapter.getData()), ArrayList(data))
        val diffResult = DiffUtil.calculateDiff(diffUtilsCallback)
        adapter.update(data)
        diffResult.dispatchUpdatesTo(adapter)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuItem = menu.add(0, -1111, 0, "Refresh")
        menuItem.setIcon(android.R.drawable.ic_menu_rotate)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == -1111) {
            presenter.loadData()
        }
        return super.onOptionsItemSelected(item)
    }
}
