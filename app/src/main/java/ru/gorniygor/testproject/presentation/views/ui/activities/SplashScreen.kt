package ru.gorniygor.testproject.presentation.views.ui.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler

class SplashScreen : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Handler().postDelayed({
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }, 1500)

    }
}