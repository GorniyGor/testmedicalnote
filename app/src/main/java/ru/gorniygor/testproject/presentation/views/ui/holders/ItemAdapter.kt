package ru.gorniygor.testproject.presentation.views.ui.holders

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.gorniygor.testproject.R
import ru.gorniygor.testproject.domain.Item

class ItemAdapter : RecyclerView.Adapter<ItemAdapter.Holder>() {

    private val data = ArrayList<Item>()

    fun update(data: List<Item>){
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun getData() = data

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_holder, parent, false)
        )
    }


    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.name.text = data[position].name
        holder.volume.text = data[position].volume.toString()
        holder.amount.text = String.format("%.2f", data[position].price.amount)

    }

    override fun getItemCount(): Int = data.size


    class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val name = view.findViewById<TextView>(R.id.tvName)
        val volume = view.findViewById<TextView>(R.id.tvVolume)
        val amount = view.findViewById<TextView>(R.id.tvAmount)
    }
}