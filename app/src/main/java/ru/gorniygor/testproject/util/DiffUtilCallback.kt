package ru.gorniygor.testproject.util

import android.support.v7.util.DiffUtil
import ru.gorniygor.testproject.domain.Item

class DiffUtilCallback (val oldList: List<Item>, val newList: List<Item>) : DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].javaClass == newList[newItemPosition].javaClass
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].name == newList[newItemPosition].name &&
                    oldList[oldItemPosition].volume == newList[newItemPosition].volume &&
                    oldList[oldItemPosition].price.amount == newList[newItemPosition].price.amount
        }
        
        override fun getOldListSize(): Int = oldList.size
        override fun getNewListSize(): Int = newList.size
    }